import React, {Component} from 'react';
import './App.css';
import InputMirror from './InputMirror.js';
import store from './store'; //It will automatically take index.js from that store folder. Also, whereever we are calling <Counter/> pass store from import and name it a var store.
//here, inputValue="hi" should come from the store.

class App extends Component{
    render(){
        return(
            <div className="App">
            <InputMirror store={store} />
            </div>
        )

    }
}

export default App;