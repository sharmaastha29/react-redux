import React from 'react';
import {connect} from 'react-redux'; //connects  state to props from redux store to react component. 
 


//create the pure fxnl component
//to enter text into the input textbox we have created input box. Here, we will see the flow of how 
//the data we enter in input textbox goes all the way from component to store and after actions are 
//dispatched how it again comes back from store to component and is rendered on the screen.

function InputMirror(props){
return(
    <div>
    <input val={props.inputValue} onChange={props.inputChanged}/> 
    <p> {props.inputValue} </p>
    </div>
    
)
}

//this state in arg of fxn comes from redux-->reducers in store.
const mapStateToProps= (state)=>{
return(
    {inputValue:state.inputValue}
) 
}

//this dispatch in arg of fxn comes from redux-->reducers in store. This inputChanged variable will be passed to props of InputMirror and will go to props.inputChanged inside onChange.
//connect mapDispatchToProps to react component via connect.
const mapDispatchToProps=(dispatch)=>{
    return( {inputChanged:(evt)=>{
        console.log('input Changed!',evt.target.value); //Now evt.target.value is gvng us whatevr we are typingin inputbox. We need to dispatch action which will have evt.target.value
        const action= {type:'INPUT_CHANGE', text:evt.target.value} ;//here, we have text also along with type. 'text' can be any name need not necessarily be text only. iT is like payload, it can be any var name. whtevr text we will write here it will go to reducer in store.
        dispatch(action);
        //Alternatively, we can also write combining 34, 35 lines together:-
        //dispatch({type:'INPUT_CHANGE', text:evt.target.value}); //This is also correct.
    }
    }

    )
}

export default connect (mapStateToProps,mapDispatchToProps)(InputMirror);

//So, now we have made all connections and hooked up all things. Now we need to create and dispatch our own action. Until now it was redux's default action.


//Conclusion: So, with every change in input (on every single key stroke)it is dispatching the action, running the reducer, changes state and component rerenders.