//create a separate fxn (here: getRepos())and pass dispatch from redux in its args so that we can use dispatch to send an action.
//We can directly write export default infront of the fxn when we create the fxn  or we can give it at the end. See alternative below.
//Also, we need to import this fxn from this file in RepoSearch.js
  function getRepos(dispatch)
 {

    let query='steak'; //passing a var called query which has some content to be searched ie: steak. we will pass this query at the end of url in fetch mtd so it will prepare a url that include the content to be searched(here: steak). It will return anything which has staek word in it. If it matches steak word it will return that. So, right now whatever we type in textbox doesn't matter bcs it will always take steak. We will correct that ltr.
    fetch(`https://api.github.com/search/repositories?q=${query}`)
    
    .then((response)=>{
     return response.json(); // here response is the same response written in then which is holding the repositories names and we are specifying the format of that response. It shld be in json format. So here we are basically converting raw response into json format.
    })
    
    .then((data)=>{ //this data variable will hold all the matching results from the repository.
    console.log('do we have data?', data);
    const action={type:'SEARCH_REPOS', payload:data.items}; //here payload is any variable in which data var which has whole response is going. out of that data we need only items array to be set to repos array. So, we will extract items array out of data variable. So, now we have got data into repos array and we now need to populate/render that array onto the screen inside the ul tag. 
    dispatch(action); //DISPATCH WILL WORK SEPARATELY INSIDE A FXN OUT OF COMPONENT ALSO.
    });


}

// Alternate way of doing export when we want to export a fxn .
export default{
    getRepos
}