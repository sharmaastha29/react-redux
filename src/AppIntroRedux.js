/*In this, we are going to see how redux fxns with react. We will see rdux majorly here.
For making any redux application, just do create-react-app to get the application ready in which we can use both react and redux coding
After that we will use npm install redux react-redux --save (this cmd installs redux library and also 
react along with redux dependencies which can easily let redux work along react. It gvs some handy 
features if we want to use react and redux together)cmd in node.js cmd prompt inside that redux 
folder so that dependencies are added in packg json file.
After adding dependency we can import redux into our application .

About redux we need to focus on 4 steps basically->
Step1. Store-requires-(state,reducer ) //create a store.To create a store it  requires 2 pieces of information; one we shld know reducers; another is state(same as in react- initial state is always there in store which will work or set up for first time when we are loading our application. thereafter we need toprovide some action to get the application working)
2. Reducer- requires-(state, action) // create a reducer. it is like an agent. In order to create reducer we req 2 infos; one is state(what is the situation now ); another is action(what action shld be taken as per state gvn)
3. Subscribe- to state //ceate subscribe// ie: connect to the base i.e: state for telling the situation: ie. get the current state.
4. Dispatch-an action //create dispatch- means the action to be taken by agent reducer. 

 so, first of all, our main task is to create store. Since store req state and reducer. State we can directly mention in store but for reducer we need to first create reducer which req state, action.
 So, order of creation goes like this:
 1.reducer(state, action)
 2.store(reducer, state)
 3. subscribe
 4. dispatch


 Now, we will write a simple code -> very simple one just to understand:- see below:
 import React from react, //ReactDom from react-dom is not reqrd bcs we are not rendering ReactDOM here. It is already rendered in idex.js page, createStore from redux. createStore helps us in creating store.
Make the component and export it also import it in index.js so that it can be rendered and also mention ur component's name inside ReactDOM.
Inside component's render mtd we will write the above 4 steps

 */

import React, {Component} from 'react';
import {createStore} from 'redux'; 


class ReduxDemo extends Component{
render(){
  //Step2: Create a reducer so as to be able to use it inside store. Reducer req 2 things: state, action.
  //Here, reducer is a constatnt storing reducer. It is in form of a fxn (can be normal or fat arrow fxn) taking 2 params state, action
  //action param is basically a cmd which shld come based on what is the state. eg: if action.type is ATTACK we might want to change the state here. type is a property which is mandatorily taken by action to tell what action is taking place and is generally in caps. 
  //ATTACK is the action and payload is variable which simply gvs some message if action ATTACK occurs. It can be anything value/ abc etc. when ATTACK action happens agent(reducer) will gv sm action which is action.payload which needs to be dispatched later
  const reducer=function(state, action){
    if(action.type==="ATTACK"){
     return action.payload
    }
    if(action.type==="GREENATTACK"){
      return action.payload
     }

    return state // if there is no action ATTACK return initial state itself bcs reducer shld nvr return undefined ; it must return something
    }

  //Step1: Create store: requires: reducer, state
  // store is a constant storing store we have created, createStore is an already defined mtd which takes 2 things: reducer, state. Peace here is the state. Generally state will be in form of object containing lots of things.
  const store=createStore(reducer,"Peace");

  // //Step2: Create a reducer so as to be able to use it inside store. Reducer req 2 things: state, action.
  // //Here, reducer is a constatnt storing reducer. It is in form of a fxn (can be normal or fat arrow fxn) taking 2 params state, action
  // //action param is basically a cmd which shld come based on what is the state. eg: if action.type is ATTACK we might want to change the state here. type is a property which is mandatorily taken by action to tell what action is taking place and is generally in caps. 
  // //ATTACK is the action and payload is variable which simply gvs some message if action ATTACK occurs. It can be anything value/ abc etc. when ATTACK action happens agent(reducer) will gv sm action which is action.payload which needs to be dispatched later
  // const reducer=function(state, action){
  // if(action.type==="ATTACK"){
  //  return action.payload
  // }
  // return state // if there is no action ATTACK return initial state itself bcs reducer shld nvr return undefined ; it must return something
  // }


  //Step3: Subscribe the store. subscribe is also a fxn which can take fat arrow fxn which can get the new value of state
  //"Store is now: " is a statement which will be printed in console and store.getState() gvs the update value of state after some actio is taken. store is used to call getstate () bcs store only holds the state/ dummy variables
  store.subscribe(()=>{
  console.log("Store is now: ", store.getState());
  })

  //Step4: Dispatch the store ie: whatever new values of state is there just dispatch that to render
  //dispatch is a mtd which takes values in form of object. First param is always type which here is ATTACK and second one is payload: anymessage which mught occur when action ATTACk occurs
  store.dispatch({type:"ATTACK", payload:"Iron Man"})
  store.dispatch({type:"GREENATTACK", payload:"Hulk"})
  return (
  <div>
  test
  </div>
  )
}

}

export default ReduxDemo; 

/*We generally place step2 above step1 bcs reducer is used in store so first we need to write that
 ---> what all actions are there*/
/* So flow is earlier store will have some dummy data which shld be printed if we do console. But
 store goes to reducer and sees the condition if action becomes something do that ie: definition 
 is written in this. These are the formal params. Then we are passing the actual params in dispatch 
 and accordingly current state of 'state' is updated bcs we have subscribed the store.*/
 /*So, in this way we have done all logic part in redux and react is only having the view part */
 /*Remember- reducer goes in a reducer folder with a separate file name, dispatch goes into a separate folder/file and then all these are linked together.  */