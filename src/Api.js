/*Here, we will see how to take input from inputbox and search the input content.*/

function getRepos(dispatch,query){
    //let query='steak'; //remove query from here and pass it directly to this fxnl component from outside. Also, this means in RepoSearch.js wherever we are calling this fxn we need to pass query from  there.
    fetch(`https://api.github.com/search/repositories?q=${query}`)
    
    .then((response)=>{
     return response.json(); 
    })
    
    .then((data)=>{ 
    console.log('do we have data?', data);
    const action={type:'SEARCH_REPOS', payload:data.items}; 
    dispatch(action); 
    });


}


export default{
    getRepos
}