/*Here, we will see url calls using ajax/axios. Later, we will do the same thing using async calls 
with the help of middlewares like: redux thunk and redux sagas. 
Here, we have called APIs using  ajax. The adv is we can do everything in mapDispatchToPorps itself but the disadv is also here. 
The disadv is mapDispatchToProps get too long as we are doing all API calls from here.and thus it is not an organised way of doing it. 
Also, since the code is inside mapDispatchToProps so it means it is not reusable separately with another component. So, we can use it separately by using the ajax call inside mapDispatchToProps in another separate file in form of a function. Check api.js */


import React from'react';
import {connect} from 'react-redux';
import Api from './Api.js';
console.log('api?', Api);


//Writing a pure fxnl component here
//if we have a functional component then we can directly export here;export default function RepoSearch(props){...}; no need to go at the end and export.
//But here we do need to do the export at the bottom bcs we need to hook this component with redux via conect
//We need to wrap our input button inside <form> tag  so that we can put onSubmit action on form so that when we type on input box and hit enter it takes as submit.
//Now, whatever we give inside  onSubmit of form we need to map it to mapDispatchToProps.
function RepoSearch(props){
 
    return (
       <div>
       <h1>Repo Search</h1>
       <form onSubmit={props.handleSubmit}>  
       <input searchValue={props.searchVal} onChange={props.InputChanged}/>
       </form>
       {/* <ul>
        <li>      repo#1    </li>
        <li>      repo#2    </li>
      </ul> */}

       <ul>
          {props.myrepos.map((repo)=>{
           return (<li key={repo.id} ><a href={repo.html_url}> {repo.name}</a> </li>) //we have put link tag and set the html field of repo array so that when clicking on repo.name it shows other details about that selected repo name.

       })}
       </ul>
        


       
       
       </div>
    )
}

const mapStateToProps=(state)=>{
return(
    {searchVal: state.searchInputValue}, //return state from store to here in RepoSearch.js
    {myrepos:state.repos}
)
}

//This is how we dispatch actions to store. dispatch is from redux.
//So, when we click on enter of input textbox handleSubmit fxn is fired. As soon as the input textbox is pressed enter in the action of handleSubmit fxn we want to call a url which shld display the matched results.
const mapDispatchToProps=(dispatch)=>{
return(

{InputChanged:(evt)=>{
console.log('changed', evt.target.value);
const action= {type:'INPUT_CHANGE', text:evt.target.value};
dispatch(action);
},

handleSubmit:(evt)=>{
    evt.preventDefault();
    
    //Commenting ajax call bcs we created a separate Api for this and using that.Below mtd is also an alternate way of doing the same.
    // //Using ajax for fetching url and making search.
    // //We can call a fetch mtd and can pass the url inside that straight away. The url which we need to pass we will take from api.github.com->repository search url====> "https://api.github.com/search/repositories?q={query}{&page,per_page,sort,order}". but we don't want that page limit so we will remove that and make query a js variable which we are passing here.
    // // Keep 'https://api.github.com/search/repositories?q={query}' inside ``(backticks) so that we can put $ sign here.and take query variable. So, remove '' and put ``.
    // //If we have matching content in repository with word steak as mentioned in the variable passed in the url, then it will return a fxn with response or else it will return error if error happens.
    // let query='steak'; //passing a var called query which has some content to be searched ie: steak. we will pass this query at the end of url in fetch mtd so it will prepare a url that include the content to be searched(here: steak). It will return anything which has staek word in it. If it matches steak word it will return that. So, right now whatever we type in textbox doesn't matter bcs it will always take steak. We will correct that ltr.
    // fetch(`https://api.github.com/search/repositories?q=${query}`)
    
    // .then((response)=>{
    //  return response.json(); // here response is the same response written in then which is holding the repositories names and we are specifying the format of that response. It shld be in json format. So here we are basically converting raw response into json format.
    // })
    
    // .then((data)=>{ //this data variable will hold all the matching results from the repository.
    // console.log('do we have data?', data);
    // const action={type:'SEARCH_REPOS', payload:data.items}; //here payload is any variable in which data var which has whole response is going. out of that data we need only items array to be set to repos array. So, we will extract items array out of data variable. So, now we have got data into repos array and we now need to populate/render that array onto the screen inside the ul tag. 
    // dispatch(action);
    // });

    Api.getRepos(dispatch);
    console.log('handling submit');
    }
    //even though we have written do we have data first in the code and handling submit after that still it is printing handling submit first and then do we have data later. This is bcs 
    //when we are hitting enter after typing in textbox it is firing handleSubmit fxn which is doing async api call to a repository's url to fetch the result. This async api call response may take 
    //sometime to complete so even though it is executed first it will be put in queue to get completed and all other sync api calls will happen first like printing handling submit is a sync 
    //call as it is executed and completed immediately and ltr on when this async api call completes it will be printed. It will not block other sync api calls bcs of async api call to complete rather
    //will put async api call in queue and execute other calls.
    //Also, we do get data after sending the aync api call in console under items. We got in form of array having 30 records at a time. Total records are 277. It has all the repos which have steak word used in it whether it is in name or username etc having unique id as key for each record. also, on expanding a particular id we will get a url to it's github repo to be able to see about it in detail.
    // We need to render this array on screen giving each record one by one.
    //Now, we need to  create an action here and also we would need to provide what will happen when this action takes place in store.
}
)
}


export default connect (mapStateToProps, mapDispatchToProps)(RepoSearch);