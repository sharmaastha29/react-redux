 import React from'react';
import {connect} from 'react-redux';
import Api from './Api.js';
console.log('api?', Api);

function RepoSearch(props){
 
    return (
       <div>
       <h1>Repo Search</h1>
       {/* <form onSubmit={props.handleSubmit}> Instead  of making direct call to handleSubmit we will now create an arrow fxn bcs of which react will not take the evt automatically as before and here we would need to pass explicitly. We have made arrow fxn so that we are able to take input value coming from evt as we want to take input entered in the textbox.  */}
       <form onSubmit={(evt)=>props.handleSubmit(evt, props.searchVal)}>
       <input searchValue={props.searchVal} onChange={props.InputChanged}/>
       </form>
       <ul>
          {props.myrepos.map((repo)=>{
           return (<li key={repo.id} ><a href={repo.html_url}> {repo.name}</a> </li>)
       })}
       </ul>
       </div>
    )
}


const mapStateToProps=(state)=>{
return(
    {searchVal: state.searchInputValue}, 
    console.log('abc',state.searchInputValue ),
    console.log('def', state.repos),
    {myrepos:state.repos}
    
)
}


const mapDispatchToProps=(dispatch)=>{
return(
{
InputChanged:(evt)=>{
console.log('changed', evt.target.value);
const action= {type:'INPUT_CHANGE', text:evt.target.value};
dispatch(action);
},

handleSubmit:(evt, query)=>{  // passing evt, query from RepoSearch fxn's handleSubmit mtd.
evt.preventDefault();
Api.getRepos(dispatch, query); //passing query from handlesubmit variable here to getRepos fxn in Api.js. Since we can't use props for inside of handleSubmit so we need to pass query param to handleSubmit as well.   
console.log('handling submit');
} 
})
}


export default connect (mapStateToProps, mapDispatchToProps)(RepoSearch);