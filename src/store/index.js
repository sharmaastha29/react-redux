import {createStore} from 'redux';

const initialState={
    repos:[] , //an array called repos which we will hold github repos from browser.
    searchInputValue:'' //To keep the value of input from input text box.
}


const reducer=(state=initialState, action)=>{
console.log('reducer', action);
switch(action.type){
case 'INPUT_CHANGE': //for InputChanged fxn
return Object.assign({},state,{searchInputValue: action.text})
case 'SEARCH_REPOS': //for handleSubmit fxn
return Object.assign({},state,{repos: action.payload}); //We are setting repos array with what is there in action's payload. Also, let's create an action in RepoSearch.js 
default:
return state;
}
}


const store=createStore(reducer);

export default store;