import {createStore} from 'redux';

const initialState={
    inputValue:''
};

//reducer shld always return state.
//You can also make anonymous fxn
const reducer=(state=initialState, action)=>
{
    console.log('reducer', action);
    //dispatching action based on criteria.
    switch(action.type){
        case 'INPUT_CHANGE': //Since in input textbox we are passing some input so it will have some length thus our action type can't be like: INCREMENT or DECREMENT; it has to INPUT_CHANGE cs we are chnaging input in this.
        return Object.assign({},state,{inputValue: action.text})  //Here, along with action type we need to send action text also in action creator ie. in mapDispatchToProps so that it can take up that input value and assign to inputValue which is further overriden to state and the brand new object{}.
        default:
        return state;   
     }


    

}



const store=createStore(reducer);

export default store;

//Now that we have created store we will now hook up this redux with react using connect in component.