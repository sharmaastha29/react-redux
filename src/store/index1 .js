/*Here, we will be creating store and for that we need reducer. So we will be creating both here.
For this, import createStore from redux to create the store. 
Store takes 2 things-1. state and 2. reducer
So,  second thing is we need to define state so that state can be taken from store. This is just the initial state/ default state for the application and can be changed depending upon what actions takes plc ltr.
Third thing is we will create a reducer.
After creating reducer , we will create store as we have both the things now state and action.
 */

import {createStore} from './node_modules/redux';

const initialState={
    count: 3
};

//reducer is in form of fxn which takes 2 things- 1. action, 2. state. Here we are making fat arrow fxn , if we want we can mk normal one also.
// we will write state=initialState bcs for the first time running we need a default state and subsequently we will have in-state.
//Also, we are console logging action. We are returning state so that it goes to store and can be passed to mapStateToProps fxn.
//First time when application is run it is redux who will be calling this fxn. From second time we will through dispatch.
const reducer=(state=initialState, action)=>{
    console.log('reducer is running', action);

    switch(action.type)  //here, switch between the actions coming from mapDispatchToProps. 'action' var shld be same as given in mapDispatchToProps()
    {
        case 'INCREMENT': //when action passed from mapDispatchToProps is INCREMENT then return a brand new updated copy of state.
            return Object.assign({}, state, {count:state.count+1} );  //If case is INCREMENT coming from action then assign updated state to new object. It cpies all all the data from state to the brand new object '{}'  so this new object will have count:3 but we need updated value so we will pass a third object {count: state.count+1} which means the new count var will have old count value and add 1 to it. So, the flow is create a brand new object-> copy all state data to brand new object and then create a third new object; increment count value to state.count(8)+1=9 and assign it to state object which in turn assigns it to first object which earlier had 8 but will now have overriden/updated value ie: 9 and return that updated object.
        case 'DECREMENT': //when action passed from mapDispatchToProps is DECREMENT then return a brand new updated copy of state. 
            return  Object.assign({},state  , {count:state.count-1} );
        default:         //if default case ie. no action then simply return state initial/prev one.
            return state;
    }
}

//Create a store by calling createStore mtd which we have already imported from redux. It will take state, reducer.
//Since, reducer already has state we can simply pass reducer. No need to pass state.
const store= createStore(reducer);


//export this store so that it can be imported and used in counter and else where.
export default store;

/*This is how we create a store. As of now, data is static and not changing. We will do that in sometime. */

/*
Entire Flow:
When the application is refreshed and run from starting first time redux will run the reducer bcs it will tk inbuilt action and not ours.
When first action is executed  the reducer runs and initial state gets stored in actual stateand it goes to component through mapStateToProps and mapDispatchToProps to Component to render so we in console we are getting messages of console: mapStateToProps and render msg. 
Now, if we go and gv our second request by clicking on increment button it prints reducer's console log clicking which means it went to reducer taking action increment from mapDispatchToProps ie: it did create action and dispatched the action and goes to reducer in store. 
It will print in console resp action since we have written console.log and it will update the state and return us brand new state object with value count=9 via mapToProps and mapToDispatch.
Flow is: component click button-> create action->dispatches action->run reducer in store->gvs output back to component
*/