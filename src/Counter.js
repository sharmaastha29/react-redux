/* we have created two components till now- one is App and other is counter ; both in different 
files->App.js and Counter.js resp. This Counter component can be a class based or funxnl component
as well. Here, we will mk it pure fxnl component.
We are taking state and handler fxns out of this component and feeding them through props so that 
will automatically take state and handlers. So, we are creating pure fxnl based component to 
maintain the design/view alone via render() mtd.  
This pure fxnl components will be in form of function and will have only return and not render()mtd.
It will take props. Let's do the desinging part-> we would need a button and a label kind of thing Count. Data how it will come and handler fxns we will separate it. 
The dynamic data like: Count value will come through props from parent App component which is there in App.js.
So, App component is parent component and Counter component is the child component. Handler for Increment button to increment the value of count we will see ltr. 
Right now we have simply static count prop in parent and passing it over to child component.
Till now was simple react. Now we will see redux-->
So, the flow is when we will click on increment button it will create some action in reducer and then it will dispatch the action when we will pass actual data and tell reducer to run depending upon that action and gv the data  to store so that it can gv to component */
//Component-> create action->dispatch action->run reducer for taking action bcs  what actions to be taken are defined in reducer-> Component via store
//Here, we will be importing connect from redux. Connect helps in glueing the react and redux together.
//Now, the first thing we need is a fxn/handler that will map state coming from store to props of Counter Component.
//For this we need to make a fxn which will take state from store and pass count variable from store to our component counter. 
//Also, we would need inbuilt 'connect' keyword which will link that fxn which is taking state from store and  to component counter . So, connect basically creates a new component which wraps our component and that mapStateToProps component. See below:-
//onClick={props.onIncrementClick} is coming as props from mapDispatchToProps and onClick is the event of button. When button is clicked this fxn coming from Redux which has updated value will go to that mapDis...() fxn and it will pass the updated value. Also, onIncrementClick is the same var as made in mapDisptachToProps which has handler function in which action is coming from redux.
//Parent is Redux child is our component and thefore redux is sending values and counter is accepting them through props. 
import React from 'react';
import {connect} from 'react-redux';


function Counter(props){
    console.log('render', props);
    return(
    <div>
        <h1>I am a counter!</h1>
        <p>Count:{props.count}</p> 
        <button onClick={props.onIncrementClick}>Increment</button>
        <button onClick={props.onDecrementClick}>Decrement</button>
    </div>

    )
}

//We have created a fxn which is mapping state from store to our counter component. const mapToState is the name we have gvn to this fxn and state is coming from store. Also, from that state in store we are extracting count variable and storing in count variable here in the fxn which is send as props to counter component.Since mapStateToProps is a fxn so it must use return keyword.
//state.count value is assigned to count variable which is being passed as props to Counter. 
function mapStateToProps(state){
           console.log('mapStateToProps', state)
           return(
            {count:state.count}
           ) 
}


//So, now that application is run for the first time, it is our turn to create an action and dispatch 
//it so that reducer in the store can run and change the state and gv the updated state to the 
//component. We do that by creating a separate fxn in the same Counter.js file. This fxn will take dispatch from redux through connect () and will simply pass on the dispacth from redux to cunter component. 
//We will create another fxn -> mapDispatchToProps for dispatching our own actions just like we created mapStateToprops
//This mapDispatchToProps fxn will take parameter dispatch coming from redux file. and we will simply return a new object
//just like we did 'count' in mapStateToProps which will be passed on as a props to Counter component.
//Generally mapDispatchToProps will contain handler mtd coming from redux and will pass on to component as props(pass the fxn as props in increment button). The object variable whch we are creating here is the one which is passed as props to component.
//Here, onIncrementClick is a variable which is holding the fxn used for incrementing data.
//Action are created in same file and dispatched also in same Counter.js component file. No need to mk separate file for that but for store ie. reducer we need to mk a separate folder and file.
//Actions are nothing but simple object in form of key:value pair which has many properties: type is one of the properties.
// In const action={type:'INCREMENT'}   ===> action is just a variable to hold action created. type is the property of action and 'INCREMENT' is the action. It is always written in CAPS within single quotes. it is bcs of this ype property of action which is INCREMENT here we will be able to look up for this INCREMENT action defined inside reducer and send corresponding payload or output.
// This same action mentioned in mapDispatchToProps will ie: INCREMENT will be looked for in Reducer and corresponding action/statement/result will be given back. 
function mapDispatchToProps(dispatch){
return(
    {onIncrementClick:()=>{
        console.log('clicking');
        const action={type:'INCREMENT'}; //Create action1
        dispatch(action);  //dispatch action; action here is the variable object made in above line ie: in create action and dispatch is the redux mtd. It will take the output from reducer in store. So, now on clicking increment button it will gv our created action in 'type' in console. So, now go in reducer and SWITCH between actions eg: INCREMENT and some other action may be.
    },

    onDecrementClick:()=>{
        const action={type:'DECREMENT'};
        dispatch(action); 
    }
}
)
}


//Here, we are linking our counter component with mapStateToProps fxn which is sending state to props in component with help of connect keyword. This conect keyword creates a new wrapper component on top of our component which will have the mapToState fxn and counter component. 
//Also, attach mapDispatchToProps which is coming from redux to Counter component.
export default connect(mapStateToProps,mapDispatchToProps )(Counter);

/*Now, we will be creating store from where state will come into mapStateToProps fxn. This store needs to be created in a separate folder called store under src and in a different file. Following this convention is mandatory.*/
/*If we go to the console we will see the comment in console saying reducer is running and action it
 will show. This is how we come to know that for the first time when the application is run redux calls
 an action and initial state is called bcs the action in console is: type:"@@redux/INIT" 
 and this will happen once when the store is created. From the next time we need to create nad dispatch action.
 So, basically first time when store is created it calls default action and sets the initial state inthe store
 and from that store state is passed to mapStateToProps fxn and from there it is passed to render mtd of Counter component.
 So, if we console in component we will be able to see the count value. So, flow remains 
 same=> req from component comes ->create action(first time when application is run redux does, 
 next time we have to do)->dispatch action-> reducer in store runs -> gvs result (state) to component.
 So, now that application is run, it is our turn to create an action and dispatch it so that reducer 
 in the store can run and change the state and gv the updated state to the component.
 Now to mk the data come dynamically we would need to have a handler fxn and create and dispatch 
 action.
 
 */